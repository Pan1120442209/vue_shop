/*
 * @Date: 2021-02-01 15:47:56
 * @LastEditors: PanFengNing
 * @LastEditTime: 2021-03-04 20:53:15
 * @FilePath: \vue_shop\src\main.js
 */
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import './plugins/element.js'
//导入全局样式表global
import './assets/css/global.css'
//导入第三方图图标库css
import './assets/fonts/iconfont.css'
//导入axios
import axios from 'axios'
//导入table tree
import treeTable from 'vue-table-with-tree-grid'
//导入富文本编辑器
import VueQuillEditor from 'vue-quill-editor'
import 'quill/dist/quill.core.css' // import styles
import 'quill/dist/quill.snow.css' // for snow theme
import 'quill/dist/quill.bubble.css' // for bubble theme
import echarts from 'echarts'

axios.defaults.baseURL = 'http://120.53.120.229:8888/api/private/v1/'
//请求拦截器(每次请求都拦截)
axios.interceptors.request.use(config => {
  config.headers.Authorization = window.sessionStorage.getItem('token')
  return config;
})
Vue.prototype.$http = axios
Vue.prototype.$echarts = echarts
Vue.config.productionTip = false
Vue.component('tree-table',treeTable)
Vue.use(VueQuillEditor, /* { default global options } */)
//注册全局过滤器
Vue.filter('dataFormat',function(originVal){
  const dt = new Date(originVal)
  const y = dt.getFullYear()
  const m = (dt.getMonth() + 1 + '').padStart(2,'0')
  const d = (dt.getDay() + '').padStart(2,'0')
  const hh = (dt.getHours() + '').padStart(2,'0')
  const mm = (dt.getMinutes() + '').padStart(2,'0')
  const ss = (dt.getSeconds() + '').padStart(2,'0')
  return `${y}-${m}-${d} ${hh}:${mm}:${ss}`
})
new Vue({
  router,
  render: h => h(App)
}).$mount('#app')

