/*
 * @Date: 2021-02-01 17:31:35
 * @LastEditors: PanFengNing
 * @LastEditTime: 2021-03-05 10:17:49
 * @FilePath: \vue_shop\src\router\index.js
 */
import Vue from 'vue'
import VueRouter from 'vue-router'
// import Login from '../components/Login.vue'
// import Home from '../components/Home.vue'
// import Welcome from '../components/welcome.vue'
// import Users from '../components/user/Users.vue'
// import Rights from '../components/power/Rights.vue'
// import Roles from '../components/power/Roles'
// import Goods from '../components/good/Goods'
// import Params from '../components/good/Params'
// import Categories from '../components/good/Categories'
// import Orders from '../components/order/Orders.vue'
// import Reports from '../components/reports/Reports.vue'
// import Add from '../components/good/Add.vue'

// 懒加载: 登录——主页——欢迎页
const Login = () => import(/* webpackChunkName: "login_Home_Welcome" */ "../components/Login.vue")
const Home = () => import(/* webpackChunkName: "login_Home_Welcome" */ '../components/Home.vue')
const Welcome = () => import(/* webpackChunkName: "login_Home_Welcome" */ '../components/index.vue')

// 用户管理
const Users = () => import(/* webpackChunkName: "users" */ "../components/user/Users.vue")
//yaconst Profile = () => import(/* webpackChunkName: "users" */ "components/user/Profile.vue")

// 权限管理
const Rights = () => import(/* webpackChunkName: "rights" */ "../components/power/Rights.vue")
const Roles = () => import(/* webpackChunkName: "rights" */ "../components/power/Roles")

// 商品管理
const Categories = () => import(/* webpackChunkName: "goods" */ "../components/good/Categories")
const Params = () => import(/* webpackChunkName: "goods" */ "../components/good/Params")
const Goods = () => import(/* webpackChunkName: "goods" */ "../components/good/Goods")
const Add = () => import(/* webpackChunkName: "goods" */ "../components/good/Add.vue")

// 订单管理
const Orders = () => import(/* webpackChunkName: "order_Report" */ "../components/order/Orders.vue")

//数据报表
const Reports = () =>import("../components/reports/Reports.vue")
Vue.use(VueRouter)

const routes = [
  {
    path:'/',
    redirect:'/login'
  },
  {
    path:'/login',
    component:Login
  },
  {
    path:'/home',
    component:Home,
    redirect:'/welcome',
    children:[ 
      {path:'/welcome',component:Welcome},
      {path:'/users',component:Users},
      {path:'/rights',component:Rights},
      {path:'/roles',component:Roles},
      {path:'/goods',component:Goods,},
      {path:'/params',component:Params},
      {path:'/categories',component:Categories},
      {path:'/orders',component:Orders},
      {path:'/reports',component:Reports},
      {path:'/goods/add',component:Add}
    ]
  },
 
]

const router = new VueRouter({
  routes,
})
//挂载路由导航守卫
router.beforeEach((to, from, next) => {
  // to 将要访问的路径
  // from 代表从哪个路径跳转而来
  //next是一个函数，表示放行
  if(to.path === '/login'){
    return next();
  }
  //获取token
  const tokenStr = window.sessionStorage.getItem('token');
  //t没有oken,强制跳转login页
  if(!tokenStr){return next('/login')}
  next();
})
export default router
